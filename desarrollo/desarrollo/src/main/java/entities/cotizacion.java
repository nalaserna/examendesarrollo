package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


@Entity
@NamedQuery(name="cotizacion.findAll", query="SELECT c FROM cotizacion c")
public class cotizacion implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	@Temporal(TemporalType.DATE)
	private Date fecha;


	@ManyToMany
	@JoinColumn(name="servicio")
	private servicio servicio;

	//bi-directional many-to-one association to Usuario
	@ManyToOne
	@JoinColumn(name="cliente")
	private usuario cliente;
	
	@ManyToOne
	@JoinColumn(name="consultor")
	private usuario consultor;

	public cotizacion() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}


	public servicio getServicio() {
		return this.servicio;
	}

	public void setServicio(servicio servicio) {
		this.servicio = servicio;
	}

	public usuario getCliente() {
		return this.cliente;
	}

	public void setCliente(usuario cliente) {
		this.cliente = cliente;
	}
	
	public usuario getConsultor() {
		return this.consultor;
	}

	public void setConsultor(usuario consultor) {
		this.consultor = consultor;
	}

}