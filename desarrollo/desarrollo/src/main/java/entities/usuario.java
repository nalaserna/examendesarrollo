package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


@Entity
@NamedQuery(name="usuario.findAll", query="SELECT c FROM usuario c")
public class usuario implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	private String nombre;
	private String apellido;
	private int tipoUsuario;
	
	
	public usuario() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setNombre(String nombre ) {
		this.nombre = nombre;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	
	public String getApellido() {
		return this.apellido;
	}
	
	public void setTipoUsuario(int tipo) {
		this.tipoUsuario=tipo;
	}
	
	public String getTipoUsuario() {
		if(this.tipoUsuario==1) {
			return "Cliente";
		}else {
			return "Consultor";
		}
	}

}