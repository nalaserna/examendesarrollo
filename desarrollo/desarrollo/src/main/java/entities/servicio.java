package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;


@Entity
@NamedQuery(name="servicio.findAll", query="SELECT c FROM servicio c")
public class servicio implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	private String descripcion;
	private String imagen;
	private String categoria;
	
	
	public servicio() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public void setDescripcion(String descripcion ) {
		this.descripcion = descripcion;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}
	
	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	
	public String getImagen() {
		return this.imagen;
	}
	
	public void setCategoria(String categoria) {
		this.categoria=categoria;
	}
	
	public String getCategoria() {
		return this.categoria;
	}
}