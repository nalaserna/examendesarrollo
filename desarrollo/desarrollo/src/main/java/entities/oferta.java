package entities;

import java.io.Serializable;
import javax.persistence.*;
import java.util.Date;
import entities.cotizacion;


@Entity
@NamedQuery(name="oferta.findAll", query="SELECT c FROM oferta c")
public class oferta implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="id")
	private int id;

	@Temporal(TemporalType.DATE)
	private Date fecha;

	private int valor;

	@OneToOne
	@JoinColumn(name="cotizacion")
	private cotizacion cotizacion;

	public oferta() {
	}

	public int getId() {
		return this.id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Date getFecha() {
		return this.fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public int getValor() {
		return this.valor;
	}

	public void setValor(int valor) {
		this.valor = valor;
	}

	public cotizacion getCotizacion() {
		return this.cotizacion;
	}


}